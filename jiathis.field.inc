<?php

/**
 * @file
 * Field related hook implementations.
 */

/**
 * Implements hook_field_info().
 */
function jiathis_field_info() {
  return array(
    'jiathis' => array(
      'label' => t('JiaThis'),
      'description' => t('This field stores jiathis settings in the database.'),
      'instance_settings' => array(),
      'default_widget' => 'jiathis_icon_widget',
      'default_formatter' => 'jiathis_icon_formatter',
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function jiathis_field_is_empty($item, $field) {
  return empty($item['value']);
}

//@todo set body field, image field in instance settings
/**
 * Implements hook_field_instance_settings_form().
 *
 * @see viewfield_field_instance_settings_form_validate()
 */
function jiathis_field_instance_settings_form($field, $instance) {
  $entity_type = $instance['entity_type'];
  $bundle = $instance['bundle'];
  
  $fields = _field_info_collate_fields(FALSE);
  $fields = $fields['instances'][$entity_type][$bundle];
  
  $body_field_options = array();
  $image_field_options = array();
  foreach ($fields as $key => $value) {
    if ($value['widget']['module'] == 'image') {
      $image_field_options[$key] = $value['label'];
    }
    if ($value['widget']['module'] == 'text') {
      $body_field_options[$key] = $value['label'];
    }
  }
  $body_field_options['none'] = 'None';
  $image_field_options['none'] = 'None';
  
  //dpm(_field_info_collate_fields(FALSE));
  //dpm($fields);
  
  $form['#field_name'] = $field['field_name'];
  //$form['custom_title']
  $form['body_field'] = array(
    '#type' => 'select',
    '#title' => t('Field for summary'),
    '#options' => $body_field_options,
  );
  $form['image_field'] = array(
    '#type' => 'select',
    '#title' => t('Field for image'),
    '#options' => $image_field_options,
  );
  
  return $form;
}

/**
 * Implements hook_field_formatter_info().
 *
 * Creates a formatter element for all the default formatter types.
 */
function jiathis_field_formatter_info() {
  $formatters = array();

  $formatters['jiathis_icon_formatter'] = array(
    'label' => t('Icons'),
    'field types' => array('jiathis'),
    'settings' => array(
      'style' => 'standard', //standard, mini
      'icon_size' => '16', //16x16, 32x32
      'show_counts' => true,
      'extra_css' => '',
      //'recommender' => false,
    ),
  );
  
  return $formatters;
}

/**
 * Implementss hook_field_formatter_settings_form()
 */
function jiathis_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  switch ($display['type']) {
    case 'jiathis_icon_formatter':
      $element['style'] = array(
        '#title' => t('Style'),
        '#type' => 'select',
        '#default_value' => $settings['style'],
        '#options' => array(
          'standard' => t('Standard'),
          'mini' => t('Mini'),
        ),
      );
      $element['icon_size'] = array(
        '#title' => t('Icon Size'),
        '#type' => 'select',
        '#default_value' => $settings['icon_size'],
        '#options' => array(
          '16' => '16 px',
          '32' => '32 px',
        ),
      );
      $element['show_counts'] = array(
        '#title' => t('Show shared counts'),
        '#type' => 'checkbox',
        '#default_value' => $settings['show_counts'],
      );
      $element['extra_css'] = array(
        '#title' => t('Extra CSS declaration'),
        '#type' => 'textfield',
        '#size' => 40,
        '#default_value' => $settings['extra_css'],
        '#description' => t('Specify extra CSS classes to apply to the button'),
      );
      //@todo icon sorting
      break;
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function jiathis_field_formatter_settings_summary($field, $instance, $view_mode) {
  $view = $instance['display'][$view_mode];
  $info = '';
  switch ($view['type']) {
    case 'jiathis_icon_formatter':
      $settings = $view['settings'];

      $style = '<b>' . $settings['style'] . '</b>';
      $icon_size = '<b>' . $settings['icon_size'] . '</b>';
      
      $info = t('Style: !style', array('!style' => $style)) . '<br />';
      $info .= t('Icon size: !icon_size', array('!icon_size' => $icon_size));
      break;
  }

  return $info;
}

/**
 * Implements hook_field_formatter_view().
 */
function jiathis_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  
  //dpm($langcode);//only support node now
  $jiathis_uid = variable_get('jiathis_uid', '');
  $jiathis_data_track_clickback = !empty($jiathis_uid) ? 'true' : 'false';
  //$language = $entity->language;
  $language = $langcode;
  $body_field = $instance['settings']['body_field'];
  if ($body_field != 'none') {
    $summary = $entity->{$body_field}[$language][0]['value'];
    if (isset($entity->{$body_field}[$language][0]['summary'])) {
      $summary = $entity->{$body_field}[$language][0]['summary'] ? $entity->body[$language][0]['summary'] : $entity->body[$language][0]['value'];
    }
  }
  //$summary = check_plain($summary);
  //$summary = drupal_html_to_text($summary);
  $summary = str_replace("\r\n", '', $summary);
  $summary = !empty($summary) ? truncate_utf8($summary, 60, FALSE, true, 1) : ''; //@todo 这个以后在字段里设置
  
  $image_field = $instance['settings']['image_field'];
  if ($image_field != 'none' && isset($entity->{$image_field}[$language])) {
    $image_uri = $entity->{$image_field}[$language][0]['uri'];
  }
  $image_url = !empty($image_uri) ? file_create_url($image_uri) : '';//dpm($entity);
  
  $title = $entity->title . ' | ' . variable_get('site_name', "");

  switch ($display['type']) {
    case 'jiathis_icon_formatter':
      $markup = '<!-- JiaThis Button BEGIN -->';
      
      if ($display['settings']['icon_size'] == '32') {
        $markup .= '<div id="jiathis_style_32x32">';
      } else {
       $markup .= '<div id="ckepop" class="clearfix">';
      }

      $markup .= '<a class="jiathis_button_tsina"></a>';
      $markup .= '<a class="jiathis_button_qzone"></a>';
      $markup .= '<a class="jiathis_button_renren"></a>';
      $markup .= '<a class="jiathis_button_kaixin001"></a>';
      $markup .= '<a class="jiathis_button_tieba"></a>';
      $markup .= '<a class="jiathis_button_print"></a>';
      $markup .= '<a href="http://www.jiathis.com/share?uid='.$jiathis_uid.'" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>';
      $markup .= '<a class="jiathis_counter_style"></a>';
      $markup .= '';
      $markup .= '</div>';
      $config = '<script type="text/javascript">
        var jiathis_config = {
          title:"'.$title.'",
          summary:"'.$summary.'",
          pic:"'.$image_url.'",
          data_track_clickback:'.$jiathis_data_track_clickback.',
        }
      </script>';
      $markup .= $config;

      if ($display['settings']['style'] == 'mini') {
        $markup .= '<script type="text/javascript" src="http://v2.jiathis.com/code_mini/jia.js?uid='.$jiathis_uid.'" charset="utf-8"></script>';
      } else {
        $markup .= '<script type="text/javascript" src="http://v2.jiathis.com/code/jia.js?uid='.$jiathis_uid.'" charset="utf-8"></script>';
      }
      
      $markup .= '<!-- JiaThis Button END -->';
      $html_output = array(
        '#markup' => $markup,
      );
      foreach ($items as $delta => $item) {
        $element[$delta] = $html_output;
      }
      
    break;
    
  }
  
  return $element;
}

/**
 * Implements hook_field_prepare_view().
 */
function jiathis_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  foreach ($items as $key => $item) {
    if (!isset($item[0]['value'])) {
      $items[$key][0]['value'] = 'Dummy value';
    }
  }
}

/**
 * Implements hook_field_presave().
 *
 * Adds a dummy value to $items to make rendering possible.
 */
function jiathis_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $dummy_value = 'Dummy value';

  if (count($items) == 0) {
    $items += array(array('value' => $dummy_value));
  }
  else {
    foreach ($items as $delta => $value) {
      $items[$delta]['value'] = $dummy_value;
    }
  }
}

/**
 * Implements hook_field_widget_info().
 */
function jiathis_field_widget_info() {
  return array(
    'jiathis_icon_widget' => array(
      'label' => t('JiaThis Icon'),
      'field types' => array('jiathis'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function jiathis_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : 'Dummy';
  $widget = $element;
  $widget['#delta'] = $delta;

  return $element;
}

