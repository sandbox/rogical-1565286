<?php

/**
 * @file
 * Beautytips settings form and submit action
 */

/**
 * Menu callback - beautytips admin settings form
 */
function jiathis_admin() {
  
  $form['jiathis_uid'] = array(
    '#type' => 'textfield',
    '#title' => t('你在JiaThis.com的用户ID'),
    '#description' => t('用户ID是统计分享数据的重要参数'),
    '#default_value' => variable_get('jiathis_uid', ''),
  );

  //$form['#submit'][] = 'beautytips_admin_submit';

  return system_settings_form($form);
}

/**
 * Submit function for beautytips settings form
 */
function jiathis_admin_submit($form, &$form_state) {
  $jiathis_uid = $form_state['values']['jiathis_uid'];
  variable_set('jiathis_uid', $jiathis_uid);
}

